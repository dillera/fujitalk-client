//
// ;*** define your project wide constants here
// 
// ;*** I like to keep memory locations at top of this file
//
PMG_ADDRESS = $6800;
LOGO_CHARSET = $6800;
DISPLAY_LIST_ADDRESS = $6900; 
CHARSET = $6C00;
VRAM_ADDRESS = $7000;

LINE_WIDTH = 40;
TAB_LINES = 51;
TAB_MAX = 5;
TAB_SIZE = LINE_WIDTH * TAB_LINES; // 34 * 40 = 1360

VIEW_HEIGHT = 20;

VRAM_SIZE = (TAB_SIZE * TAB_MAX) + 32; // 1360 * 5 = $1aa0 
VRAM_STATUS = VRAM_ADDRESS + VRAM_SIZE; //
VRAM_STATUS2 = VRAM_ADDRESS + VRAM_SIZE + LINE_WIDTH; //

INPUT_SIZE = 160;

VRAM_INPUTS = VRAM_STATUS2 + LINE_WIDTH; // 

VRAM_INPUT0 = VRAM_INPUTS + (INPUT_SIZE * 0) + 1;
VRAM_INPUT1 = VRAM_INPUTS + (INPUT_SIZE * 1) + 2;
VRAM_INPUT2 = VRAM_INPUTS + (INPUT_SIZE * 2) + 3;
VRAM_INPUT3 = VRAM_INPUTS + (INPUT_SIZE * 3) + 4;
VRAM_INPUT4 = VRAM_INPUTS + (INPUT_SIZE * 4) + 5;

VRAM_TAB0 = VRAM_ADDRESS;
VRAM_TAB1 = VRAM_ADDRESS + TAB_SIZE;
VRAM_TAB2 = VRAM_ADDRESS + $1000;
VRAM_TAB3 = VRAM_ADDRESS + TAB_SIZE + $1000;
VRAM_TAB4 = VRAM_ADDRESS + $2000;

BUFFER_ADDRESS = $A000;

INPUT_MAX = 159;

THEME_LEN = 11;
THEME_COUNT = 5;

INPUT_IDLE = 0;
INPUT_KEY = 1;
INPUT_ENTER = 2;
INPUT_TAB = 3;
INPUT_HELP = 4;
INPUT_SELECT = 5;
INPUT_START = 6;
INPUT_OPTION = 7;
INPUT_HIDE = 8;

INPUT_QUIT = $ff;

I_SPACE = 12;
I_LEFT = 13;
I_RIGHT = 14;
I_UP = 15;
I_DOWN = 16;
I_BOTTOM = 17;

NONE = $FF;

INPUT_DELAY = 16;

APPKEY_CREATOR_ID = $b0c1;
APPKEY_APP_ID = $03;
APPKEY_SERVER_KEY = $5e;
APPKEY_AUTH_KEY = $a0;
APPKEY_CONFIG_KEY = $c0;


TOKEN_ERROR = 'E';
TOKEN_USER = 'U';
TOKEN_STATUS = 'S';
TOKEN_LINES = 'L';

ERROR_LOGIN_FAILED = 41;
ERROR_SIGNUP_FAILED = 42;
ERROR_UNAUTHORIZED = 43;
ERROR_TAB_LIMIT = 44;
ERROR_COMMAND_UNKNOWN = 45;
ERROR_USER_UNKNOWN = 46;
ERROR_NO_SUCH_WINDOW = 47;


STATUS_REFRESH_INTERVAL = 250;

VBAR_LEFT = 208;
PAGER_HEIGHT = 80;
PAGER_OFFSET = PMG_ADDRESS + $180 + 16;

SPINNER_LEFT = 123;
SPINNER_TOP = 60;

